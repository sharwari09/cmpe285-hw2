## Name: Sharwari Phadnis
## Student ID: 012168884
## CMPE 285 Homework 2

from flask import Flask
from flask import render_template
from flask import request
from datetime import datetime, timedelta
from collections import OrderedDict
import pytz
import requests


app = Flask(__name__)
query_response = OrderedDict()
API_KEY = "52KLQYELXEU3NBJ1"


@app.route('/', methods=['GET'])
def home():
    return render_template('financeinfo.html')


@app.route('/queryfinance', methods=['POST'])
def queryFinanceInfo():
    stock = request.form['ticketSymbol']
    time_series_url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=" \
                      + stock + "&interval=5min&apikey={}".format(API_KEY)
    r = requests.get(time_series_url)
    time_series_info = r.json()
    if "Error" in time_series_info:
        return render_template('errorinfo.html')
    elif "None" in time_series_info:
        return render_template('callsexceeded.html')
    else:
        print("Time Series Request Successful")

    stock_meta_url = "https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=" +\
                     stock + "&apikey={}".format(API_KEY)
    r = requests.get(stock_meta_url)
    meta_data = r.json()
    stock_info = [stock, meta_data['bestMatches'][0]["2. name"], meta_data['bestMatches'][0]["8. currency"]]
    today = time_series_info['Meta Data']['3. Last Refreshed'].split(" ")[0]
    endDate = (datetime.strptime(today, '%Y-%m-%d') - timedelta(days=1)).strftime('%Y-%m-%d')
    query_response["Name"] = stock_info[1]
    query_response["Symbol"] = stock_info[0]
    query_response["Currency"] = stock_info[2]

    currentDateInfo = time_series_info['Time Series (Daily)'][today]
    change_by_value = round(float(currentDateInfo['4. close']) -
                            float(time_series_info['Time Series (Daily)'][endDate]['4. close']),2)
    change_by_percent = round((change_by_value /
                               float(time_series_info['Time Series (Daily)'][endDate]['4. close']))*100,2)
    if change_by_value < 0:
        change_by_value_str = str(change_by_value)
        change_by_percent_str = str(change_by_percent)
    else:
        change_by_value_str = "+" + str(change_by_value)
        change_by_percent_str = "+" + str(change_by_percent)
    timeInPDT = datetime.now(pytz.timezone('US/Pacific'))
    formatted_date = (timeInPDT.strftime('%a-%b-%d-%H-%M-%S-%Y').split("-"))
    query_response['Time'] = " ".join(formatted_date[:3]) + " " + ":".join(formatted_date[3:6]) \
                             + " PDT " + formatted_date[6]
    query_response['Stock Values'] = "$" + str(currentDateInfo['4. close']) + " " \
                                     + change_by_value_str + "(" + change_by_percent_str + "%" + ")"

    return render_template('financeinfo2.html', query_response=query_response)


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
